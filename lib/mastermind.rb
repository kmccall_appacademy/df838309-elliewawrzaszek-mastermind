class Code

  PEGS = {
    "b" => "blue",
    "o" => "orange",
    "y" => "yellow",
    "g" => "green",
    "r" => "red",
    "p" => "purple"
  }
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    pegs[idx]
  end

  def self.parse(string)
    pegs = string.split("").map do |ch|
      raise "error" unless PEGS.keys.include?(ch.downcase)
      PEGS[ch.downcase]
    end
    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.values.shuffle.first}
    Code.new(pegs)
  end

  def exact_matches(other_code)
    count = 0
    @pegs.each_index do |idx|
      if @pegs[idx] == other_code[idx]
        count += 1
      end
    end
    count
  end

  def near_matches(other_code)
    near = []
    @pegs.each_index do |idx|
       next if @pegs[idx] == other_code[idx]
       if @pegs.include?(other_code[idx])
         near << other_code[idx]
      end
    end
    near.uniq.length
  end

  def ==(other_code)
    return true if exact_matches(other_code) >= 4
    false
  end

end

class Game
  MAX_TURNS = 10

  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def play
    MAX_TURNS.times do
      guess = get_guess

      if guess == @secret_code
        puts "Nice work!"
        return
      end

      display_matches(guess)
    end

    puts "Maybe you'll figure it out someday :-/"
    puts "I'm so disappointed."
  end


  def get_guess
    puts "Make a guess..."
    guess_input = gets.chomp
      begin
        Code.parse(guess_input)
      rescue
        puts "error!"
        retry
      end
  end

  def display_matches(other_code)
    near = @secret_code.near_matches(other_code)
    exact = @secret_code.exact_matches(other_code)
    print "You have:\n"
    print "#{near} near matches\n"
    print "#{exact} exact matches.\n"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
